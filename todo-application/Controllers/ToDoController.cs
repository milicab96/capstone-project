﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Net;
using todo_application.Models;

namespace todo_application.Controllers
{
    public class ToDoController : Controller
    {
        private readonly ILogger<ToDoController> _logger;
        private readonly TodoDbContext _db;

        public ToDoController(ILogger<ToDoController> logger, TodoDbContext todoDbContext)
        {
            _logger = logger;
            _db = todoDbContext;
        }

        public IActionResult Index()
        {
            var lists = _db.TodoLists.Include(x => x.Items).ToList();
			return View(lists);
        }

        [Route("/list/{listId}")]
        public IActionResult List(int listId)
        {
            var list = _db.TodoLists.Include(x => x.Items).FirstOrDefault(x => x.Id == listId);
			if (list is null)
			{
				return NotFound();
			}
			return View(list);
        }

        [HttpGet]
        [Route("/create")]
        public IActionResult Create()
        {
            return View();
        }

        [Route("/create")]
        [HttpPost]
        public IActionResult Create(TodoList todoList)
        {
            if(!ModelState.IsValid)
            {
                return View();
            }
            _db.TodoLists.Add(todoList);
            _db.SaveChanges();
            var id = todoList.Id;
            return Redirect($"create/{id}");
        }

		[HttpGet]
		[Route("/create/{listId}")]
		public IActionResult CreateItems(int listId)
        {
			var list = _db.TodoLists.Include(x => x.Items).FirstOrDefault(x => x.Id == listId);
			if (list is null)
			{
				return NotFound();
			}
			ViewBag.ToDoList = list;
			return View();
        }

        [Route("/create/{listId}")]
        [HttpPost]
        public IActionResult CreateItems(int listId,TodoItem todoItem)
        {
            var list = _db.TodoLists.Include(x => x.Items).FirstOrDefault(x => x.Id == listId);
            if(list is null)
            {
                return NotFound();
            }
            ViewBag.ToDoList = list;
            todoItem.TodoList = list;
			_db.TodoItems.Add(todoItem);
            _db.SaveChanges();
			ModelState.Clear();
			return View();
        }

        [HttpGet]
        [Route("/edit/{id}")]
		public IActionResult Edit(int id)
		{
            var list = _db.TodoLists.FirstOrDefault(x => x.Id == id);
			if (list is null)
			{
				return NotFound();
			}
            return View(list);
		}

        [HttpPost]
		[Route("/edit/{id}")]
		public IActionResult Edit(int id, TodoList todoList)
		{
            var list = _db.TodoLists.FirstOrDefault(x => x.Id == id);
			if (list is null)
			{
				return NotFound();
			}
            list.Name = todoList.Name;
            _db.SaveChanges();
            return RedirectToAction("Index");
		}

		[HttpGet]
		[Route("/item/{id}")]
		public IActionResult EditItem(int id)
		{
			var item = _db.TodoItems.Include(x => x.TodoList).FirstOrDefault(x => x.Id == id);
			if (item is null)
			{
				return NotFound();
			}
			return View(item);
		}

		[HttpPost]
		[Route("/item/{id}")]
		public IActionResult EditItem(int id, TodoItem todoItem)
		{
			var item = _db.TodoItems.Include(x => x.TodoList).FirstOrDefault(x => x.Id == id);
			if (item is null)
			{
				return NotFound();
			}
            item.Title = todoItem.Title;
            item.Description = todoItem.Description;
            item.DueDate = todoItem.DueDate;
            item.Status = todoItem.Status;
			_db.SaveChanges();
			return RedirectToAction("Index");
		}

        [HttpGet]
        public IActionResult ChangeStatus(int id)
        {
			var item = _db.TodoItems.Include(x => x.TodoList).FirstOrDefault(x => x.Id == id);
            return View(item);
        }

        [HttpPost]
        public IActionResult ChangeStatus(int id, TodoItem todoItem)
        {
			var item = _db.TodoItems.Include(x => x.TodoList).FirstOrDefault(x => x.Id == id);
			if (item is null)
			{
				return NotFound();
			}
            item.Status = todoItem.Status;
            _db.SaveChanges();  
            return Redirect($"/list/{item.TodoList.Id}");
        }

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}