﻿using System.ComponentModel.DataAnnotations;

namespace todo_application.Models
{
    public class TodoItem
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime CreationDate { get; private set; }

        public TodoItemStatus Status { get; set; }

        public TodoList TodoList { get; set; }

        public TodoItem()
        {
            CreationDate = DateTime.Now;
            Status = TodoItemStatus.NotStarted;
        }
    }
    public enum TodoItemStatus
    {
        NotStarted,
        InProgress,
        Completed
    }
}
