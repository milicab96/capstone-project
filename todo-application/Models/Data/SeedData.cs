﻿using Microsoft.EntityFrameworkCore;

namespace todo_application.Models.Data
{
    public static class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            TodoDbContext context = app.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<TodoDbContext>();

            context.Database.EnsureCreated();

            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }
            if (!context.TodoLists.Any())
            {
                context.TodoLists.AddRange(
                    new TodoList
                    {
                        Name = "Grocery List",
                        Items = new List<TodoItem>
                        {
                        new TodoItem { Title = "Buy milk", Description = "1 gallon of whole milk", DueDate = new DateTime(2024, 12, 31), Status = TodoItemStatus.NotStarted },
                        new TodoItem { Title = "Buy bread", Description = "1 loaf of whole wheat bread", DueDate = new DateTime(2024, 12, 31), Status = TodoItemStatus.NotStarted },
                        new TodoItem { Title = "Buy eggs", Description = "1 dozen large eggs", DueDate = new DateTime(2024, 12, 31), Status = TodoItemStatus.NotStarted }
                        }
                    },
                    new TodoList
                    {
                        Name = "House Cleaning List",
                        Items = new List<TodoItem>
                        {
                        new TodoItem { Title = "Vacuum living room", Description = "Vacuum the carpet in the living room", DueDate = new DateTime(2024, 12, 31), Status = TodoItemStatus.NotStarted },
                        new TodoItem { Title = "Dust furniture", Description = "Dust all the furniture in the house", DueDate = new DateTime(2024, 12, 31), Status = TodoItemStatus.NotStarted },
                        new TodoItem { Title = "Mop kitchen floor", Description = "Mop the kitchen floor with a wet mop", DueDate = new DateTime(2024, 12, 31), Status = TodoItemStatus.NotStarted }
                        }
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
