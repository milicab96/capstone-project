﻿using Microsoft.EntityFrameworkCore;

namespace todo_application.Models
{
    public class TodoDbContext : DbContext
    {
        public TodoDbContext(DbContextOptions<TodoDbContext> options) : base(options) { }
        public DbSet<TodoList> TodoLists { get; set; }
        public DbSet<TodoItem> TodoItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TodoList>()
                .HasMany(e => e.Items)
                .WithOne(e => e.TodoList)
                .IsRequired(false);

            modelBuilder.Entity<TodoList>()
                .HasIndex(x => x.Name)
                .IsUnique();

            modelBuilder.Entity<TodoItem>()
                .HasOne(e => e.TodoList)
                .WithMany(e => e.Items);
        }


    }
}
