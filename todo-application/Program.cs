using Microsoft.EntityFrameworkCore;
using todo_application.Models;
using todo_application.Models.Data;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
var connectionString = builder.Configuration.GetConnectionString("ToDoConnection");
builder.Services.AddDbContext<TodoDbContext>(options => options.UseSqlServer(connectionString));

var app = builder.Build();
SeedData.EnsurePopulated(app);

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
	app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
	name: "edit",
	pattern: "edit/{id}",
	defaults: new { controller = "ToDo", action = "Edit" });

app.MapControllerRoute(
	name: "editItems",
	pattern: "item/{id}",
	defaults: new { controller = "ToDo", action = "EditItems" });

app.MapControllerRoute(
	name: "createItems",
	pattern: "create/{listId}",
	defaults: new { controller = "ToDo", action = "CreateItems" });

app.MapControllerRoute(
	name: "default",
	pattern: "{controller=ToDo}/{action=Index}/{id?}");


app.Run();
